import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FormOfInc } from 'src/app/interfaces/forms';
import { passwordValidator } from 'src/app/validators/password-validator';


@Component({
  selector: 'app-register-material',
  templateUrl: './register-material.component.html',
  styleUrls: ['./register-material.component.scss']
})
export class RegisterMaterialComponent implements OnInit {
/*   formOfInc: FormOfInc[] = [
    { value: 'LE', viewValue: 'Legal entity' },
    { value: 'IE', viewValue: 'Individual entrepreneur' }
  ]
 */
  form: FormGroup = new FormGroup({
    account: new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
      passwordConfirm: new FormControl(null, Validators.required)
    },
      [passwordValidator]
    ),
    profile:new FormGroup({
      name: new FormControl(''),
      phone: new FormControl(''),
      city: new FormControl('')
    }),
    company: new FormGroup({
      companyName: new FormControl(null, Validators.required),
      formOfInc: new FormControl('LE',  Validators.required),
      iNN: new FormControl(null, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
      kPP: new FormControl(null, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
      oKPO: new FormControl(null, [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      foundationDate: new FormControl(null,)
    }),
    contacts: new FormArray([])
  });
  submit(): void{
    if (this.form.invalid) {
      return;
    }
    //const { email, password, passwordConfirm } = this.form.get('account')?.value;
    //console.log('Отправляю', email, password, passwordConfirm);
    console.log(this.form.value);
  }
  constructor() { }
  addContact() {
    (this.form.get('contacts') as FormArray).push(
      new FormGroup({
        contactName: new FormControl(null, Validators.required),
        contactPosition: new FormControl(null, Validators.required),
        contactPhone: new FormControl(null, Validators.required)
      })
    );
  }

  removeContact(index: number) {
    (this.form.get('contacts') as FormArray).removeAt(index);
  }

  getContactsControl() {
    return (this.form.get('contacts') as FormArray).controls;
  }

  toggleKPPValidator(): void {
    this.form.get('company.formOfInc')?.valueChanges.subscribe(
      (companyFormOfInc: string) => {
        if (companyFormOfInc === 'IE') {
          this.form.get('company.kPP')?.clearValidators();
          this.form.get('company.kPP')?.reset();
          return;
        }
        
        this.form.get('company.kPP')?.setValidators([
          Validators.required,
          Validators.minLength(9),
          Validators.maxLength(9)
        ])
      }
    );
  }

  ngOnInit(): void {
    this.toggleKPPValidator();
    //console.log(this.form);
    
  }
}
function index(index: any) {
  throw new Error('Function not implemented.');
}

