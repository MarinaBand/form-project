import { AbstractControl, ValidationErrors } from "@angular/forms";

export const passwordValidator = (control: AbstractControl): ValidationErrors | null => {
    const pw1: string = control.get('password')?.value;
    const pw2: string = control.get('passwordConfirm')?.value;
    const error: ValidationErrors | null = pw1 === pw2
        ? null
        : { passwordsNotEqual: true };
    control.get('passwordConfirm')?.setErrors(error);
    return error;

};